# utilities

list of utilities docker images

## NCFTP

No configuration needed simply run the ncftp command as usual. Check the documentation from the official site.

## GSutil

To configure GSutil you need to get an HMAC key to access the bucket (create a service account with Storage Admin and HMAC Key Admin, generate an HMAC key in the Settings->Interoperability in Cloud Storage).

```
[Credentials]
gs_access_key_id = <access_key>
gs_secret_access_key = <secret_key>

[Boto]
https_validate_certificates = True

[GSUtil]
content_language = en
default_api_version = 1
default_project_id = <project_id>
```

replace the access_key, secret_key and project_id with the right values. Place the content of the file in an environment variable for the Gitlab CI/CD. The type of the variable must be a File.

Before using GSutil run this command

```bash
cp $VARIABLE_NAME /root/.boto
```

now you are setup to use the GSutil image.

## Heroku

To configure Heroku run the command ```heroku login -i``` and insert the email and the password or

```bash
export HEROKU_API_KEY=<token>
docker run -it --rm -e HEROKU_API_KEY=$HEROKU_API_KEY registry.gitlab.com/therickys93/utilities/heroku
```

## DOCTL

```bash
export DIGITALOCEAN_ACCESS_TOKEN=<token>
docker run -it --rm -e DIGITALOCEAN_ACCESS_TOKEN=$DIGITALOCEAN_ACCESS_TOKEN -v $HOME/.ssh/:/root/.ssh doctl
$ doctl compute droplet list
$ doctl compute ssh --ssh-key-path /root/.ssh/id_rsa_do user@droplet
```
